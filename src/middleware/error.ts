import { NextFunction, Request, Response } from "express";

export default (err:any, req:Request, res:Response, next:NextFunction) => {
  if (err){
    res.status(err.statusCode || 500).json({
      success: false,
      error  : err.message,
      code   : 4004,
    });

    throw err;
  }
};
