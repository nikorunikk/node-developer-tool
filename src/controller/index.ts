import schemaCheck from "../utils/schemaCheck";
import { Response, Request, NextFunction } from "express";


export default async (controllerName:string) => {
  const { main, schema } = await import(`./${controllerName}.controller`);
  return async (req:Request, res: Response, next:NextFunction) => {
    try {
      if (schema){
        await schemaCheck(schema, req);
      }
      await main(req, res);
    } catch (err){
      next(err);
    }
  };
};
