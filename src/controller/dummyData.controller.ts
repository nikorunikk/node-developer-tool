import { Response, Request } from "express";
import db from "../database/models";
// import { MyError } from "../utils/errors";
import Joi from "joi";

export const schema = Joi.object({
  section: Joi.string().required()
});
export const main = async (req:Request, res: Response) => {
  const user =await db.User.findAll({});
  console.log(user);
  res.send(user);
};


