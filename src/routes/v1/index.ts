import express, { Router } from "express";
import controller from "../../controller";
const router = Router();
const app = express();

export default async () => {
  router.get("/dummy", await controller("dummyData"));
  return app.use("/v1", router);
};
