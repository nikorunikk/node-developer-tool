import { Request } from "express";
import { MyError } from "./errors";
import Joi from "joi";

export default async (schema:any, req:Request) => {
  let value = await schema.validateAsync(req.query);
  if (Joi.isError(value)){
    value = await schema.validateAsync(req.body);
    if (Joi.isError(value)){
      value = await schema.validateAsync(req.params);
      if (Joi.isError(value)){
        throw new MyError(value, 400);
      }
    }
  }
};
