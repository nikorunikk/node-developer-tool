import fs from "fs";

const path = `${__dirname}/../database/migration`;
(async () => {
  const files = fs.readdirSync(path, { withFileTypes: true }).filter(dirent => dirent.isFile());
  files.forEach(async element => {
    const { migrate } = await import(`${path}/${element.name}`);

    migrate();
  });
})();