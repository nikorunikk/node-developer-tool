import fs from "fs";

const path = `${__dirname}/../database/seeder`;
(async () => {
  const files = fs.readdirSync(path, { withFileTypes: true }).filter(dirent => dirent.isFile());
  files.forEach(async element => {
    const { seeder } = await import(`${path}/${element.name}`);

    seeder();
  });
})();