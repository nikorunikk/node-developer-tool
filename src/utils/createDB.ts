import fs from "fs";
import readline from "readline";
import color from "./consoleColor";

const rl = readline.createInterface({
  input : process.stdin,
  output: process.stdout
});


rl.question("DB_NAME ?. \n", (name: string) => {
  rl.question("DB_USER ?. \n", (user: string) => {
    rl.question("DB_SERVER ?. \n", (server: string) => {
      rl.question("DB_PASSWORD. \n", (pswd: string) => {
        const model_template = `DB_NAME="${name}"
DB_HOST="${server}"
DB_USER="${user}"
DB_PASSWORD="${pswd}"
`;
        fs.appendFile(`${__dirname}/../../.env`, model_template, (err) => {
          if (err) throw err;

          console.log(color.FgGreen, "DB config created");
          process.exit(0);
        });
      });
    });
  });
});
