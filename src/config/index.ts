import dotenv from "dotenv";
dotenv.config();

export default {
  server: {
    app    : process.env.APP_NAME || "My Project",
    secrect: process.env.SECRET_KEY || "base64:VGVubWFuZ2E=",
    version: process.env.APP_VERSION || "v1",
    port   : process.env.APP_PORT || 8080
  },
  database: {
    db_dialect: process.env.DB_DIALECT || "sqlite",
    db_storage: `${__dirname}/../database/database.sqlite`,
    user      : process.env.DB_USER || "postgres",
    name      : process.env.DB_NAME || "postgres",
    host      : process.env.DB_HOST || "http://localhost",
    port      : process.env.DB_PORT || "5436",
    password  : process.env.DB_PASSWORD || ""
  }
};