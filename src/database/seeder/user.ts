import User from "../models/user.model";
import Profile from "../models/profile.model";

export const seeder = async () => {
  const profile = await Profile.create(
    {
      firstname: "ADMIN",
      lastname : "ADMIN",
      age      : 99,
      nickname : "TEN_ADMIN",
      avatar   : "Somethjsahdkashkdshkjdhskjd"
    }
  );

  await User.create(
    {
      profile_id      : profile.get({ plain: true }).id,
      username        : "SomeUsername",
      email           : "something@gmail.com",
      user_status     : "ACTIVE",
      user_status_date: new Date(),
    }
  );
  Profile.hasOne(User);
  const user = await Profile.findOne({ include: User });
  if (!user) throw new Error("SOMETHING_WENT_WRONG");

  console.log(user.get({ plain: true }).user);
};