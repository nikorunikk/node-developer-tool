import User from "../models/user.model";
import Profile from "../models/profile.model";

export const migrate = async () => {
  await User.drop();
  await User.sync({ force: true });
  await Profile.drop();
  await Profile.sync({ force: true });
};