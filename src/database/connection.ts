import { Sequelize } from "sequelize";
import config from "../config";
const { database } = config;

let sequelize = new Sequelize({
  storage: database.db_storage,
  dialect: "sqlite"
});

switch (database.db_dialect){
  case "postgres":
    sequelize = new Sequelize(database.name, database.user, database.password, {
      host   : database.host,
      dialect: "postgres"
    });
    break;
  default:
    sequelize = new Sequelize({
      storage: database.db_storage,
      dialect: "sqlite",
    });
}

export { sequelize } ;