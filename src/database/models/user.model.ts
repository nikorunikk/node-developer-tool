import { sequelize } from "../connection";
import seq_default from "../default";
import Profile from "./profile.model";
import { UUIDV4, DataTypes } from "sequelize";


export default sequelize.define("user", {
  id: {
    type        : DataTypes.STRING(45),
    primaryKey  : true,
    defaultValue: UUIDV4
  },
  profile_id: {
    type      : DataTypes.STRING(45),
    allowNull : false,
    references: {
      model: Profile,
      key  : "id"
    }
  },
  username: {
    allowNull: false,
    type     : DataTypes.STRING(45)
  },
  email: {
    allowNull: false,
    type     : DataTypes.STRING(500)
  },
  user_status: {
    type     : DataTypes.STRING(45),
    allowNull: true
  },
  user_status_date: {
    type        : DataTypes.DATE,
    defaultValue: new Date
  }

}, seq_default);