import { sequelize } from "../connection";
import seq_default from "../default";
import { UUIDV4, DataTypes } from "sequelize";

export default sequelize.define("profile", {
  id: {
    type        : DataTypes.STRING(45),
    primaryKey  : true,
    defaultValue: UUIDV4
  },
  firstname: {
    allowNull: false,
    type     : DataTypes.STRING(45)
  },
  lastname: {
    allowNull: false,
    type     : DataTypes.STRING(45)
  },
  avatar: {
    allowNull: false,
    type     : DataTypes.STRING(500)
  },
  nickname: {
    allowNull: true,
    type     : DataTypes.STRING(45)
  },
  age: {
    type        : DataTypes.NUMBER,
    defaultValue: new Date
  }
}, seq_default);