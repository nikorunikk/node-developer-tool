import { InitOptions } from "sequelize";
import { sequelize } from "./connection";
export default <InitOptions>{
  timestamps     : true,
  createdAt      : "created_at",
  updatedAt      : "update_at",
  deletedAt      : "deleted_at",
  paranoid       : true,
  underscored    : true,
  freezeTableName: true
};