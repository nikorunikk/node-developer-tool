import express from "express";
import bodyParser from "body-parser";
import fileUpload from "express-fileupload";
import cors from "cors";
import morgan from "morgan";

import middleware from "../middleware";
import color from "../utils/consoleColor";
import router from "../routes/v1";
import header from "./header";
import { sequelize } from "../database/connection";
import config from "../config";

export default (async () => {
  const routes = await router();
  const app = express();
  app.use(bodyParser.json());
  app.use(fileUpload());
  app.use(cors());
  app.use(morgan("dev"));

  app.use(header);
  app.use(routes);

  app.use(middleware.errorHandler);

  const PORT = config.server.port;
  app.listen(PORT, () => {
    console.log(color.FgBlue, `⚡️[server]: Listening ${PORT}`);
  });
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
});
