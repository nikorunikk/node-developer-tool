import { Request, Response, NextFunction } from "express";

export default (req: Request, res:Response, next:NextFunction) => {
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  res.header("Access-Control-Allow-Credentials", "true");

  next();
};